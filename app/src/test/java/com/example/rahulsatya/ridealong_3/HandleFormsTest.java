package com.example.rahulsatya.ridealong_3;

import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.*;

/**
 * Created by rahulsatya on 13/03/18.
 */
public class HandleFormsTest {
    @Test
    public void findaRide() throws Exception {
        String to = "Dublin 1";
        String from = "Dublin 2";
        int price = 20;
        int seats = 2;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy");
        LocalDate date = LocalDate.parse("12/04/18", formatter);
        LocalTime time = LocalTime.parse("10:15");
        String repeat = "daily";
        boolean output;
        boolean expected = true;
        HandleForms handleForms = new HandleForms();
        output = handleForms.findaRide(to, from, price, seats, date, time, repeat);

        assertEquals(expected, output);
    }

    @Test
    public void offeraRide() throws Exception {

        String to = "Dublin 1";
        String from = "Dublin 2";
        int price = 20;
        int seats = 2;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy");
        LocalDate date = LocalDate.parse("12/04/18", formatter);
        LocalTime time = LocalTime.parse("10:15");
        String repeat = "daily";
        boolean output;
        boolean expected = true;
        HandleForms handleForms = new HandleForms();
        output = handleForms.findaRide(to, from, price, seats, date, time, repeat);

        assertEquals(expected, output);
    }

}