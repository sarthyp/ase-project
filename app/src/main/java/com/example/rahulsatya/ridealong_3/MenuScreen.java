package com.example.rahulsatya.ridealong_3;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MenuScreen extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{


    ImageButton imageButton1;
    Button LogoutButton;
    DrawerLayout drawer;
    NavigationView navigationView;
    Toolbar toolbar = null;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sharedEditor;


    GoogleApiClient mGoogleApiClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_screen);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        ViewStub stub = (ViewStub)findViewById(R.id.stub);

        //stub.setInflateId(R.id.activity1);
        stub.setLayoutResource(R.layout.content_menu_screen);
        stub.inflate();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedEditor = sharedPreferences.edit();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        LogoutButton = (Button)header.findViewById(R.id.logoutButton);
        LogoutButton.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View view) {
                //Auth.GoogleSignInApi.signOut()
                Intent i = new Intent(MenuScreen.this, MainActivity.class);
                startActivity(i);
                finish();

            }
        });
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);

        Intent in = getIntent();

        String name = sharedPreferences.getString("name", "");
        String email = sharedPreferences.getString("email", "");
        String img_url = sharedPreferences.getString("img_url", "");

        final TextView userName = (TextView) headerView.findViewById(R.id.userName);
        userName.setText(name);

        final TextView userEmail = (TextView) headerView.findViewById(R.id.userEmail);
        userEmail.setText(email);

        final ImageView profilePic = (ImageView) headerView.findViewById(R.id.profilePic);


        if(!img_url.equals(" "))
            Glide.with(this).load(img_url).into(profilePic);



        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.content_menu_screen, null);

        RelativeLayout contentView = (RelativeLayout) view.findViewById(R.id.contentScreen);

        ImageButton imageButton = (ImageButton) contentView.findViewById(R.id.rt_companion);

        if (imageButton == null)
            System.out.println("kata");
        else
            System.out.println("nahi kata");

        imageButton1 = (ImageButton) contentView.findViewById(R.id.carpool);

        try {
            sendDataToServer(name, email);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void sendDataToServer(String name, String email) throws IOException {

        System.out.println("inside");

        JSONObject postData = new JSONObject();
        try {
            postData.put("name", name);
            postData.put("email", email);

            new SendDeviceDetails().execute("http://ec2-34-243-244-65.eu-west-1.compute.amazonaws.com/add", email);//postData.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }



        /*RequestBody formBody = new FormBody.Builder()
                //.add("name", name)
                .add("email", email)
                .build();
        Request request = new Request.Builder()
                .url("http://ec2-34-243-244-65.eu-west-1.compute.amazonaws.com/add")
                .post(formBody)
                .build();
        OkHttpClient client = new OkHttpClient();

        Response response = client.newCall(request).execute();
        if(response.code() == 200){
            System.out.println("inside2");
            String responseData = response.body().string();
            //Process the response Data
        }else{
            System.out.println("inside3");
            //Server problem
        }*/
    }
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.carpool:
                //signIn();
                Intent i = new Intent(MenuScreen.this, RideShare.class);
                startActivity(i);
                break;
            case R.id.rt_companion:
                //signIn();
                Intent j = new Intent(MenuScreen.this, RtCompanion.class);
                startActivity(j);
                break;

        }
    }
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)
        {
            this.moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        switch (id){
            case R.id.nav_home:
                Intent i = new Intent(MenuScreen.this, MenuScreen.class);
                startActivity(i);
                break;
            case R.id.nav_ride_share:
                Intent j = new Intent(MenuScreen.this, RideShare.class);
                startActivity(j);
                break;
            case R.id.nav_rt_companion:
                Intent k = new Intent(MenuScreen.this, RtCompanion.class);
                startActivity(k);
                break;
            case R.id.nav_logout:
                MainActivity.testLogout();
                Intent l = new Intent(MenuScreen.this, MainActivity.class);
                startActivity(l);
                break;
            default:
                break;
        }




        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private class SendDeviceDetails extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            String data = "";

            HttpURLConnection httpURLConnection = null;
            try {

                httpURLConnection = (HttpURLConnection) new URL(params[0]).openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json");
                httpURLConnection.setRequestProperty("Accept", "application/json");
                httpURLConnection.setDoOutput(true);
                JSONObject jo = new JSONObject();
                jo.put("email",params[1]);
                DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
                wr.writeBytes(jo.toString());
                wr.flush();
                wr.close();

                System.out.println("asdasd");
                InputStream in = httpURLConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(in);


                int inputStreamData = inputStreamReader.read();
                while (inputStreamData != -1) {
                    char current = (char) inputStreamData;
                    inputStreamData = inputStreamReader.read();
                    data += current;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }

            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("TAG", result); // this is expecting a response code to be sent from your server upon receiving the POST data
        }
    }

}

