package com.example.rahulsatya.ridealong_3;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class RideShare extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sharedEditor;
    Button LogoutButton;
    DrawerLayout drawer;
    NavigationView navigationView;

    TabLayout tabLayout;
    ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_share);

        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.content_ride_share, null);

        viewPager=(ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(new CustomAdapter(getSupportFragmentManager(),getApplicationContext()));

        tabLayout=(TabLayout) findViewById(R.id.viewPager);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }
        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        ViewStub stub = (ViewStub)findViewById(R.id.stub);

        //stub.setInflateId(R.id.activity1);
        stub.setLayoutResource(R.layout.content_ride_share);
        stub.inflate();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedEditor = sharedPreferences.edit();



        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        LogoutButton = (Button)header.findViewById(R.id.logoutButton);
        LogoutButton.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View view) {
                //Auth.GoogleSignInApi.signOut()
                Intent i = new Intent(RideShare.this, MainActivity.class);
                startActivity(i);
                finish();

            }
        });

        View headerView = navigationView.getHeaderView(0);


        String name = sharedPreferences.getString("name", "");
        String email = sharedPreferences.getString("email", "");
        String img_url = sharedPreferences.getString("img_url", "");

        final TextView userName = (TextView) headerView.findViewById(R.id.userName);
        userName.setText(name);

        final TextView userEmail = (TextView) headerView.findViewById(R.id.userEmail);
        userEmail.setText(email);

        final ImageView profilePic = (ImageView) headerView.findViewById(R.id.profilePic);
        if(!img_url.equals(" "))
            Glide.with(this).load(img_url).into(profilePic);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id){
            case R.id.nav_home:
                Intent i = new Intent(RideShare.this, MenuScreen.class);
                startActivity(i);
                break;
            case R.id.nav_ride_share:
                Intent j = new Intent(RideShare.this, RideShare.class);
                startActivity(j);
                break;
            case R.id.nav_rt_companion:
                Intent k = new Intent(RideShare.this, RtCompanion.class);
                startActivity(k);
                break;
            default:
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    private class CustomAdapter extends FragmentPagerAdapter {
        private String fragments [] = {"Fragment 1","Fragment 2"};

        public CustomAdapter(FragmentManager supportFragmentManager, Context applicationContext) {
            super(supportFragmentManager);
        }
        @Override
        public android.support.v4.app.Fragment getItem(int position)
        {
            switch(position){
                case 0:
                    return new Fragment1();
                case 1:
                    return new Fragment2();
                default:
                    return null;
            }
        }
        @Override
        public int getCount(){
            return fragments.length;
        }
        @Override
        public CharSequence getPageTitle(int position)
        {
            return fragments[position];
        }

    }
}
