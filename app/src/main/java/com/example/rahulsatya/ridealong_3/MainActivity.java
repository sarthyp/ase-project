package com.example.rahulsatya.ridealong_3;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import android.support.v4.view.ViewPager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {


    private SignInButton SignIn;
    private GoogleApiClient googleApiClient;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sharedEditor;
    private static final int REQ_CODE = 9001;

    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        viewPager=findViewById(R.id.viewpager);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedEditor = sharedPreferences.edit();


        View someView = findViewById(R.id.signIn);

        // Find the root view
        View root = someView.getRootView();

        // Set the color
        root.setBackgroundColor(Color.rgb(255,209,2));

        SignIn = (SignInButton)findViewById(R.id.login_button);

        SignIn.setOnClickListener(this);

        ///----- These will be used to get the user details like email and profile pictures.
        GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                //.requestIdToken(getString(R.string.project_id))
                .requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this,this).addApi(
                Auth.GOOGLE_SIGN_IN_API,signInOptions
        ).build();

    }


    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.login_button:
                signIn();
                break;

        }
    }

    private void signIn()
    {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent, REQ_CODE);
    }

    private void handleResult(GoogleSignInResult result)
    {
        if(result.isSuccess())
        {
            GoogleSignInAccount account = result.getSignInAccount();
            ////-------- Can use these variables to update account infor when creating an account page and a menu ------
            String name = account.getDisplayName();
            String email = account.getEmail();
            String img_url = " ";
            if((account.getPhotoUrl().toString()) != null)
            {
                img_url = account.getPhotoUrl().toString();
            }

            sharedEditor.putString("name", name);
            sharedEditor.putString("email", email);
            sharedEditor.putString("img_url", img_url);
            sharedEditor.commit();
            Intent i = new Intent(MainActivity.this, MenuScreen.class);
            i.putExtra("name", name);
            i.putExtra("email", email);
            i.putExtra("img_url", img_url);
            startActivity(i);
            //--finish();
            //updateUI(true);
        }
        else {
            //updateUI(false);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)
        {
            this.moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public static void testLogout ()
    {

        //Auth.GoogleSignInApi.signOut();
        System.out.println("hahahaha");
    }
    private void updateUI(boolean isLogin)
    {
        if(isLogin)
        {
            //edit here
        }

        else
        {
            //homeScreen.setVisibility(View.GONE);
            SignIn.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQ_CODE)
        {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleResult(result);
        }
    }
}
